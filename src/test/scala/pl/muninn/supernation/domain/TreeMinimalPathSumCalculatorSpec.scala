package pl.muninn.supernation.domain

import cats.Id
import pl.muninn.supernation.test.{InputStreamOps, TestSpec}

class TreeMinimalPathSumCalculatorSpec extends TestSpec {

  import InputStreamOps._

  trait Setup {
    val calculator = new TreeMinimalPathSumCalculator.Default[Id]()
  }

  "TreeMinimalPathSumCalculator.createResult" should "fail if array is empty" in new Setup {
    an[Exception] should be thrownBy calculator.createResult(Array.empty, Vector.empty)
  }

  "TreeMinimalPathSumCalculator.createResult" should "return correct, lower value" in new Setup {
    calculator.createResult(Array((1, 0), (5, 1)), Vector(10)) shouldBe(Vector(10, 1), Some(0))
  }

  "TreeMinimalPathSumCalculator.calculatePossibleMoves" should "return correct values for middle node" in new Setup {
    val results1 = calculator.calculatePossibleIndexes(1, Array((3, 0), (8, 1), (5, 2)))
    results1 should contain allElementsOf (List(0, 1))

    val results2 = calculator.calculatePossibleIndexes(2, Array((3, 0), (8, 1), (5, 2)))
    results2 should contain allElementsOf (List(1, 2))
  }

  "TreeMinimalPathSumCalculator.calculatePossibleMoves" should "return correct values for far left node" in new Setup {
    val results = calculator.calculatePossibleIndexes(0, Array((3, 0), (8, 1), (5, 2)))
    results should contain allElementsOf (List(0))
  }

  "TreeMinimalPathSumCalculator.calculatePossibleMoves" should "return correct values for far right node" in new Setup {
    val results = calculator.calculatePossibleIndexes(3, Array((3, 0), (8, 1), (5, 2)))
    results should contain allElementsOf (List(2))
  }

  "TreeMinimalPathSumCalculator.calculatePossibleMoves" should "return correct values if there is only one node left" in new Setup {
    val results = calculator.calculatePossibleIndexes(3, Array((7, 0)))
    results should contain allElementsOf (List(0))
  }

  "TreeMinimalPathSumCalculator.calculate" should "return value 18 for test1.txt" in new Setup {
    val data = fileInputStream("test1.txt")
    val result: Vector[Int] = calculator.calculate(data)
    result should contain theSameElementsInOrderAs (Vector(7, 6, 3, 2))
    result.sum shouldBe 18
  }

  "TreeMinimalPathSumCalculator.calculate" should "return value 11 for test2.txt" in new Setup {
    val data = fileInputStream("test2.txt")
    val result: Vector[Int] = calculator.calculate(data)
    result should contain theSameElementsInOrderAs (Vector(2, 3, 5, 1))
    result.sum shouldBe 11
  }

  "TreeMinimalPathSumCalculator.calculate" should "return value 13 for test3.txt" in new Setup {
    val data = fileInputStream("test3.txt")
    val result: Vector[Int] = calculator.calculate(data)
    result should contain theSameElementsInOrderAs (Vector(3, 4, 2, 1))
    result.sum shouldBe 10
  }
}
