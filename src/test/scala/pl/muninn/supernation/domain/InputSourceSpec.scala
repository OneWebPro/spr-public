package pl.muninn.supernation.domain

import java.nio.file.Paths

import cats.effect.IO
import pl.muninn.supernation.test.TestSpec

import scala.io.Source

class InputSourceSpec extends TestSpec {

  trait Setup {
    val path = "test1.txt"
    val lines = Source.fromResource(path).getLines().to(LazyList).reverse.toList
    val filePath = Paths.get(getClass.getResource(s"/$path").toURI)
  }

  "FileReader.lines" should "read return file lines in reverse order" in new Setup {
    val fileReader = new InputSource.FileReader(filePath)
    val result = fileReader.lines().use(stream => IO(stream.toList)).unsafeRunSync()
    result should contain theSameElementsInOrderAs(lines)
  }

  "ReverseFileReader.lines" should "read return file lines in reverse order" in new Setup {
    val fileReader = new InputSource.ReversedLinesFileReader(filePath)
    val result = fileReader.lines().use(stream => IO(stream.toList)).unsafeRunSync()
    result should contain theSameElementsInOrderAs(lines)
  }
}
