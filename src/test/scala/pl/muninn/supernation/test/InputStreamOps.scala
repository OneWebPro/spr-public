package pl.muninn.supernation.test

import cats.Id
import cats.effect.{Bracket, ExitCase, Resource}
import pl.muninn.supernation.domain.InputSource
import cats.syntax.all._
import scala.io.Source

object InputStreamOps {
  def fileInputStream(path: String): InputSource[Id] = new InputSource[Id] {
    override def lines(): Resource[Id, LazyList[String]] =
      Resource.pure(Source.fromResource(path).getLines().to(LazyList)).map(_.reverse)
  }

  implicit val b: Bracket[Id, Throwable] = new Bracket[Id, Throwable] {
    override def bracketCase[A, B](acquire: Id[A])(use: A => Id[B])(release: (A, ExitCase[Throwable]) => Id[Unit]): Id[B] = {
      val source = acquire
      val result = use(source)
      release(source, ExitCase.Completed)
      result
    }

    override def pure[A](x: A): Id[A] = x

    override def flatMap[A, B](fa: Id[A])(f: A => Id[B]): Id[B] = f(fa)

    override def tailRecM[A, B](a: A)(fn: A => Id[Either[A, B]]): Id[B] =
      fn(a) match {
        case Right(b) => b.pure[Id]
        case Left(aa) => tailRecM(aa.pure[Id])(fn)
      }

    override def raiseError[A](e: Throwable): Id[A] = throw e

    override def handleErrorWith[A](fa: Id[A])(f: Throwable => Id[A]): Id[A] = ???
  }
}
