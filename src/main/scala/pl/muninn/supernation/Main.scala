package pl.muninn.supernation

import java.nio.file.Path

import cats.effect.{ExitCode, IO}
import cats.syntax.all._
import com.monovore.decline._
import com.monovore.decline.effect.CommandIOApp
import pl.muninn.supernation.domain.{InputSource, TreeMinimalPathSumCalculator}

object Main
    extends CommandIOApp(
      name = "tmspc",
      header = "Tree minimal path sum calculator",
      version = "0.0.1"
    ) {

  import cats.effect.Console.io._

  val pathToFile = Opts.option[Path]("file", "Path to file with tree", "f", "file").orNone

  val reverseFileReader =
    Opts.flag("runtime-reverse", "Should read file normally then reverse it in memory or read file from end to start (default)", "r").orFalse

  override def main: Opts[IO[ExitCode]] =
    (pathToFile, reverseFileReader).mapN {
      case (path, runtimeReverse) =>
        val reader = path match {
          case Some(path) => if (runtimeReverse) new InputSource.FileReader(path) else new InputSource.ReversedLinesFileReader(path)
          case None       => new InputSource.InputReader()
        }

        val calculator = new TreeMinimalPathSumCalculator.Default[IO]()

        calculator
          .calculate(reader)
          .flatMap(result => putStrLn(s"Minimal path is: ${result.mkString(" + ")} = ${result.sum}"))
          .map(_ => ExitCode.Success)
    }
}
