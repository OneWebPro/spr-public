package pl.muninn.supernation.domain

import cats.Monad
import cats.effect.Bracket
import cats.instances.all._
import cats.syntax.all._

trait TreeMinimalPathSumCalculator[F[_]] {
  def calculate(inputSource: InputSource[F]): F[Vector[Int]]
}

object TreeMinimalPathSumCalculator {

  type Tree = LazyList[String]
  type Node = (Int, Int)
  type Result = (Vector[Int], Option[Int])

  class Default[F[_]: Monad]()(implicit bracket: Bracket[F, Throwable]) extends TreeMinimalPathSumCalculator[F] {

    override def calculate(inputSource: InputSource[F]): F[Vector[Int]] =
      inputSource
        .lines()
        .use { tree =>
          tree
            .map(mapLineToNodes)
            .foldLeftM[F, Result]((Vector.empty[Int], None)) {
              case ((actualPath, lastIndex), nodes) =>
                lastIndex match {
                  case None => createResult(nodes, actualPath) // If we don't have start index, just create result and starts from 0
                  case Some(index) =>
                    val possibleIndexes = calculatePossibleIndexes(index, nodes) // Calculate possible indexes
                    val possibleMoves: Array[Node] = calculatePossibleMoves(nodes, possibleIndexes) // Filter nodes to possible indexes
                    createResult(possibleMoves, actualPath) // Get smaller one
                }
            }
        }
        .map {
          case (value, _) => value.reverse
        }

    private[supernation] def createResult(nodes: Array[Node], actualPath: Vector[Int]): F[Result] =
      nodes.minOption match {
        case Some((length, lastIndex)) => bracket.pure(actualPath :+ length, Some(lastIndex))
        case None                      => bracket.raiseError(new Exception("There was no possible moves! Something went wrong"))
      }

    private[supernation] def calculatePossibleIndexes(index: Int, nodes: Array[Node]): Vector[Int] =
      if (nodes.length == 1) Vector(0)
      else if (index > nodes.length - 1) Vector(nodes.length - 1)
      else Vector(index - 1, index)

    private def calculatePossibleMoves(nodes: Array[Node], possibleIndexes: Vector[Int]) =
      nodes.collect {
        case (value, nodeIndex) if possibleIndexes.contains(nodeIndex) => (value, nodeIndex)
      }

    private def mapLineToNodes(line: String): Array[Node] =
      line.split(" ").map(_.trim.toInt).zipWithIndex
  }

}
