package pl.muninn.supernation.domain

import java.io.{BufferedReader, InputStreamReader, FileReader => JavaFileReader}
import java.nio.charset.Charset
import java.nio.file.Path

import cats.effect.{IO, Resource}
import org.apache.commons.io.input.{ReversedLinesFileReader => ApacheReversedLinesFileReader}

import scala.jdk.CollectionConverters._

// Represents source of data - it also handles the proper order of lines
trait InputSource[F[_]] {
  def lines(): Resource[F, LazyList[String]]
}

object InputSource {

  class InputReader() extends InputSource[IO] {

    // We need to revert stream because of input stream can't be read from backward to forward
    override def lines(): Resource[IO, LazyList[String]] =
      inputReader.evalMap(readAllLines).map(_.reverse)

    def inputReader: Resource[IO, BufferedReader] =
      Resource.fromAutoCloseable(IO {
        new BufferedReader(new InputStreamReader(System.in))
      })

    def readAllLines(bufferedReader: BufferedReader): IO[LazyList[String]] =
      IO.delay {
        bufferedReader.lines().iterator().asScala.to(LazyList)
      }
  }

  class FileReader(path: Path) extends InputSource[IO] {

    // We need to revert stream because of input stream can't be read from backward to forward
    override def lines(): Resource[IO, LazyList[String]] =
      fileReader.evalMap(readAllLines).map(_.reverse)

    def fileReader: Resource[IO, BufferedReader] =
      Resource.fromAutoCloseable(IO {
        new BufferedReader(new JavaFileReader(path.toFile))
      })

    def readAllLines(bufferedReader: BufferedReader): IO[LazyList[String]] =
      IO.delay {
        bufferedReader.lines().iterator().asScala.to(LazyList)
      }
  }

  class ReversedLinesFileReader(path: Path) extends InputSource[IO] {

    override def lines(): Resource[IO, LazyList[String]] =
      fileReader.evalMap(toStream)

    def fileReader: Resource[IO, ApacheReversedLinesFileReader] =
      Resource.fromAutoCloseable(IO {
        new ApacheReversedLinesFileReader(path, Charset.defaultCharset())
      })

    def toStream(reader: ApacheReversedLinesFileReader): IO[LazyList[String]] =
      IO(LazyList.continually(reader.readLine()).takeWhile(_ != null))
  }

}
