### Assumptions to task

1. Application should allow loading data from a file or input stream 
2. Logic should be separated from CLI client
3. We are reading tree from bottom to top because it's a tree structure and to find in most optimal way for shortest path we need to look from bottom to top
3. We have 3 readers - from input stream, simple from a file and for big files (reads in reverse order, from last line to first)
    1. For input stream and normal file reader we need to materialise stream to reverse it - in theory we could change algorithm to read from top to bottom, but it would make algorithm much more complicated, and we could finish with even more memory usage because we would need to calculate EVERY possible path and find shortest
    2. **Default** reader is reverse file reader
5. Code should be tested in most crucial parts but there is no sense to test compiler and libraries
6. Code should be easy to read and maintain + easy to debug
