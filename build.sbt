name := "supernation"

version := "0.1"

scalaVersion := "2.13.2"

organization := "pl.muninn"

scalacOptions := Seq(
  "-deprecation",
  "-encoding",
  "UTF-8",
  "-feature",
  "-language:existentials",
  "-language:higherKinds",
  "-unchecked",
  "-Ywarn-dead-code",
  "-Ywarn-numeric-widen",
  "-Xfatal-warnings"
)

val catsEffectVersion = "2.1.3"
val console4CatsVersion = "0.8.1"
val cats = Seq(
  "org.typelevel" %% "cats-effect" % catsEffectVersion,
  "dev.profunktor" %% "console4cats" % console4CatsVersion,
)

val scalaTestVersion = "3.1.2"
val scalaMockVersion = "4.4.0"
val scalaTest = Seq(
  "org.scalactic" %% "scalactic" % scalaTestVersion,
  "org.scalamock" %% "scalamock" % scalaMockVersion % Test,
  "org.scalatest" %% "scalatest" % scalaTestVersion % Test
)

val declineVersion = "1.0.0"
val decline = Seq(
  "com.monovore" %% "decline" % declineVersion,
  "com.monovore" %% "decline-effect" % declineVersion,
)

val apache = Seq(
  "commons-io" % "commons-io" % "2.7"
)

libraryDependencies ++= (cats ++ decline ++ apache ++ scalaTest)

enablePlugins(JavaAppPackaging)