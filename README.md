# Triangle Test BE

To run build application:

* `./build/supernation-0.1/bin/supernation --help` to see help 

* `./build/supernation-0.1/bin/supernation --file <path_to_file>` to load file for example `./build/supernation-0.1/bin/supernation --file ./data/test1.txt`

Or you can stream data like:

```bash
cat << EOF | ./build/supernation-0.1/bin/supernation
7
6 3
3 8 5
11 2 10 9
EOF
```

To build application run:

`sbt universal:packageBin` 

To run from sbt:

* `sbt "run --file <path_to_file>"` for example `sbt "run --file ./data/test1.txt"`